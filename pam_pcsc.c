#include <sys/param.h>
#include <sys/types.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <utmp.h>

#define PAM_SM_AUTH
#include <security/pam_modules.h>
#include <security/pam_client.h>
#include "config.h"
#include "log.h"
#include "pcsc_auth.h"
#include "rfshadow.h"

#ifndef PAM_EXTERN
#define PAM_EXTERN
#endif





PAM_EXTERN int
pam_sm_authenticate (pam_handle_t *pamh, int flags,int argc, const char *argv[])
{
	const char		*service;
	const char		*user;
	char option[16];
	int reader_n=READER_N;

	char *password = NULL;
	int pam_err;

	struct utmp	utsearch;
	struct utmp	*utent;
	const char	*from;
	const char		*tty;
	const char		*rhost;
	int			i;	
	uid_t		uid,euid;


	for(i=1; i<argc; i++) {
		if(strlen(argv[i]) < 16) {
			strcpy(option,argv[i]);
			if(option[0] != '-') {
				continue;
			}
		} else {
			continue;
		}
		switch(option[1]) {
		case 'r':
			if(i+1>=argc) continue;
			reader_n=atoi(argv[i+1]);
			i++;
			break;

		case 'v':
			VERBOSE=1;
			break;
		}
	}


	/* identify user */
	uid=getuid();
	euid=geteuid();
	if ((pam_err = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS || !user || !*user)
	{
		log_error("Unable to retrieve PAM user name.\n",NULL);
		return (pam_err);
	}
	log_debug("User identified: User: %s, UID: %d, EUID: %d\n",user,uid,euid);
	if ((pam_err = pam_get_item(pamh, PAM_SERVICE,(const void **)(const void *)&service)) != PAM_SUCCESS || !user || !*user)
	{
		log_error("Unable to retrieve PAM service name.\n",NULL);
		return (pam_err);
	}

	
	log_debug("pam_pcsc %s auth start...\n",VERSION);
	log_info("Authentication request for user \"%s\" (%s)\n",user, service);

	/* check if local */

	if (pam_get_item(pamh, PAM_TTY,
				(const void **)(const void *)&tty) == PAM_SUCCESS)
	{
		if (tty && !strcmp(tty, "ssh"))
		{
			log_debug("SSH Authentication, aborting.\n",NULL);
			return (PAM_AUTH_ERR);
		}
	}
	
	log_debug("Checking whether the caller is local or not...\n",NULL);

/* Some tty checks... */

	from = ttyname(STDIN_FILENO);
	if (!from || !(*from))
	{
		log_debug("Couldn't retrieve the tty name.\n",NULL);
/*	return (1); */
	} else {
		if (!strncmp(from, "/dev/", strlen("/dev/")))
			from += strlen("/dev/");
		log_debug("Authentication request from tty %s\n", from);
	
		strncpy(utsearch.ut_line, from, sizeof(utsearch.ut_line) - 1);
		setutent();
		utent = getutline(&utsearch);
		endutent();
		if (!utent)
		{
			log_debug("No utmp entry found for tty \"%s\"\n",
					from);
			goto CONTINUE;
		}
		for (i = 0; i < 4; ++i)
		{
			if (utent->ut_addr_v6[i] != 0)
			{
				log_error("Remote authentication request: %s\n", utent->ut_host);
				log_debug("Remote login denied. \n",NULL);
				return (PAM_CRED_INSUFFICIENT);
			}
		}
	}
CONTINUE:	
	if ( ( pam_err = pam_get_item(pamh, PAM_RHOST,(const void **)(const void *)&rhost) )  != PAM_SUCCESS  )
	{
		log_error("Unable to retrieve the PAM RHOST.\n",NULL);
		return (pam_err);
	}
	if (rhost && strlen(rhost) > 0)
	{
		log_debug("Remote login denied. \n",NULL);
		return (PAM_CRED_INSUFFICIENT);
	}
	
	log_debug("Caller is local (good)\n",NULL);

	/* read config */

	if (!config_readfile(CONF_FILE))
	{
		log_info("Error reading configuration file %s. Using defaults.\n",CONF_FILE);
	}

	/* auth call*/

	if((pam_err=pcsc_auth_user(reader_n,user,service,&password)) == PAM_SUCCESS)
	{
		pam_err = pam_set_item(pamh, PAM_AUTHTOK, password);
	}
	if(NULL != password && strlen(password) > 0) { memset(password,'\0',strlen(password)); free(password); }

	return (pam_err);
}


PAM_EXTERN int
pam_sm_setcred(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_acct_mgmt(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_open_session(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_close_session(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SUCCESS);
}

PAM_EXTERN int
pam_sm_chauthtok(pam_handle_t *pamh, int flags,
	int argc, const char *argv[])
{

	return (PAM_SERVICE_ERR);
}

#ifdef PAM_MODULE_ENTRY
PAM_MODULE_ENTRY("pam_pcsc");
#endif

#ifdef PAM_STATIC

struct pam_module _pam_usb_modstruct = {
	"pam_pcsc",
	pam_sm_authenticate,
	pam_sm_setcred,
	pam_sm_acct_mgmt,
	pam_sm_open_session,
	pam_sm_close_session,
	pam_sm_chauthtok
};

#endif
