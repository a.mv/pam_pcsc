/*
 * Copyright (c) 2003-2007 Andrea Luzzardi <scox@sig11.org>
 * https://github.com/aluzzardi/pam_usb/
 *
 * This file is part of the pam_usb project. pam_usb is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAM_LOG_H_
# define PAM_LOG_H_
# define log_debug(s, ...) __log_debug(__FILE__, __LINE__, s, ##__VA_ARGS__)

extern unsigned int VERBOSE;

void __log_debug(const char *file, int line, const char *fmt, ...);
void log_error(const char *fmt, ...);
void log_info(const char *fmt, ...);


#endif /* !PAM_LOG_H_ */
