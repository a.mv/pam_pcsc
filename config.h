#ifndef _CONFIG_H
#define _CONFIG_H

#ifdef DEBUG_FILES
#define CONF_FILE "./pam_pcsc.conf"
#define SHADOW_FILE "./rfshadow"
#define USER_ID_FILE "./user_rfshadow"
#else

#ifndef CONF_FILE
#define CONF_FILE SYSCONFDIR"/pam_pcsc.conf"
#endif

#ifndef SHADOW_FILE
#define SHADOW_FILE SYSCONFDIR"/rfshadow"
#endif

#ifndef USER_ID_FILE
#define USER_ID_FILE ".rfshadow"
#endif

#endif

extern char * APDU_GET_ID;
extern char * APDU_READ_BLOCK;
extern char * APDU_WRITE_BLOCK;
extern int READER_N;

extern char * REG_FILE;
extern char * IDP_FILE;

int config_readfile(char* file);

#endif
