/*
 * Copyright (c) 2003-2007 Andrea Luzzardi <scox@sig11.org>
 * https://github.com/aluzzardi/pam_usb/
 *
 * This file is part of the pam_usb project. pam_usb is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdarg.h>
#include <string.h>
#include "log.h"

#ifdef DEBUG
unsigned int VERBOSE=1
#else
unsigned int VERBOSE=0;
#endif

static void _log_syslog(int level,const char *format, va_list ap)
{
	int i;
	char *fmt;
	fmt=strdup(format);
	openlog("pam_pcsc", LOG_PID, LOG_AUTH);
	for ( i = 0; i< strlen(fmt)-1 ; ++i )
		if ( fmt[i] == '\n') fmt[i]=';';
	vsyslog(level, fmt, ap);
	closelog();
	free(fmt);
}

static void _log_output(int level, const char *format, va_list ap)
{
	if (!isatty(fileno(stdin)) || ! VERBOSE)
		return ;
	if (level == LOG_ERR)
		fprintf(stderr, "\033[01;31m*\033[00m ");
	else if (level == LOG_NOTICE)
		fprintf(stderr, "\033[01;32m*\033[00m ");
	else
			fprintf(stderr, "* ");
	vfprintf(stderr, format, ap);
}

void __log_debug(const char *file, int line, const char *fmt, ...)
{
	va_list	ap;
#ifndef DEBUG
		return ;
#endif
	fprintf(stderr, "[%s:%03d] ", file, line);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	va_start(ap, fmt);
	_log_syslog(LOG_DEBUG, fmt, ap);
	va_end(ap);
}

void log_error( const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	_log_output(LOG_ERR, fmt, ap);
	va_end(ap);

	va_start(ap, fmt);
	_log_syslog(LOG_ERR, fmt, ap);
	va_end(ap);
}

void log_info( const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	_log_output(LOG_NOTICE, fmt, ap);
	va_end(ap);

	va_start(ap, fmt);
	_log_syslog(LOG_NOTICE, fmt, ap);
	va_end(ap);

}
