#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "pcsc.h"
int main(int argc, char *argv[])
{
	char option[256];

	int reader_n;

	PCSC_CONTEXT PCSCctx;
	CARD_ID cid;
	int n;

	reader_n=1;
	for(n=1;n<argc-1;n++)
	{
		strcpy(option,argv[n]);
		if(option[0] != '-') { continue; }
		switch(option[1]) {
			case 'r':
				reader_n=atoi(argv[n+1]);
				n++;
			break;
		}
	}

	if(pcsc_init_ctx(reader_n, &PCSCctx)<1) {fprintf(stderr,"PCSC context init failed\n"); return -1;}
	cid=pcsc_read_card_id(&PCSCctx);
	if(cid.uid_len == 0) {fprintf(stderr,"Card read failed\n"); return -1;}
	pcsc_free_ctx( &PCSCctx);

	printf("UID:%s\n",bytes2hex(cid.uid_bytes,cid.uid_len));
	printf("ATR:%s\n",bytes2hex(cid.atr_bytes,cid.atr_len));
	free_card_id(cid);
	return  0;
}
