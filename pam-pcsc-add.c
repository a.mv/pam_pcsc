#if ! defined(__sun)
#define _BSD_SOURCE     /* Get getpass() declaration from <unistd.h> */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE   /* Get crypt() declaration from <unistd.h> */
#endif
#endif

#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <pwd.h>
#include <shadow.h>
#include <errno.h>

#include "config.h"
#include "pcsc.h"
#include "pwcrypt.h"
#include "pcsc_auth.h"

#define BACKSPACE 127
#define MAX_PASSWORD_LEN 256
#define USAGE fprintf(stderr,"Usage: %s [-r <reader number>] [-s <service name>] [-m <method>] username\n",argv[0])

char * mgetpass (FILE *stream, char * prompt)
{
	char *lineptr;
	struct termios old, new;
	int nread = 0;
	char c;

	lineptr=malloc(1*sizeof(char));

	/* Turn echoing off and fail if we can't. */
	if (tcgetattr (fileno (stream), &old) != 0) return NULL;
	new = old;
	new.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL | ICANON);
	if (tcsetattr (fileno (stream), TCSAFLUSH, &new) != 0) return NULL;

	if(prompt != NULL) fprintf(stderr,"%s",prompt);

	/* Read the password. */
	while ((c = getchar()) != '\n' && nread + 1 < MAX_PASSWORD_LEN) {
		if (c == BACKSPACE) {
			if (nread > 0) {
				nread--;
				fprintf(stderr,"\b \b");
			}
		} else {
			lineptr=realloc(lineptr,nread+2);
			lineptr[nread++] = c;
			fprintf(stderr,"*");
		}
	}
	fprintf(stderr,"\n");
	lineptr[nread] = '\0';

	/* Restore terminal. */
	(void) tcsetattr (fileno (stream), TCSAFLUSH, &old);
	return lineptr;
}

int main(int argc, char *argv[])
{
	char option[256];
	char *method="id";
	char *user=NULL;
	char *service_s="*";
	char *crypt_password;

	PCSC_CONTEXT PCSCctx;
	CARD_ID ID;

	char *id_crypt;
	char *id_scrypt;
	char *id_salted;
	char *salt_ptr;
	char *IV_s;
	char *key_s;
	char *crypt_value;
	char *p_tmp;
	unsigned int crypt_len;
	char salt[2];
	CRYPT_STRUCT	crypt_data;

	int n;
	struct passwd *pwd;
	struct spwd *spwd;
	int reader_n=0;
	char *pass = NULL;

	for(n=1; n<argc; n++) {
		strcpy(option,argv[n]);
		if(option[0] != '-') {
			user=argv[n];
			continue;
		}
		switch(option[1]) {
		case 'r':
			if(n+1>=argc) continue;
			reader_n=atoi(argv[n+1]);
			n++;
			break;

		case 's':
			if(n+1>=argc) continue;
			service_s=strdup(argv[n+1]);
			n++;
			break;
		case 'm':
			if(n+1>=argc) continue;
			method=strdup(argv[n+1]);
			n++;
			break;
		default:
			USAGE;
			return 1;
		}
	}


	if(user==NULL) {
		USAGE;
		return 1;
	}

	/* read config */
	if (!config_readfile(CONF_FILE)) {
		fprintf(stderr,"Error reading configuration file %s. Using defaults.\n",CONF_FILE);
	}

	fprintf(stderr,"User: %s",user);
	if ((pwd = getpwnam(user)) == NULL) {
		fprintf(stderr," - UNKNOWN\n");
		return -1;
	}
	fprintf(stderr," \n");

	/* Reading passfords for methods other than id */
	if (strcmp(method,"id") != 0) {
		pass=mgetpass(stdin,"Password: ");
		/*pass=getpass("Password: "); */

		if ( strlen(pass) > 0 ) {
			spwd = getspnam(user);
			if ((spwd == NULL) && errno == EACCES ) {
				fprintf(stderr,"Unable to read shadow password\n");
				return -1;
			} else { /* If there is a shadow password record  */
				pwd->pw_passwd = spwd->sp_pwdp;     /* Use the shadow password  */
			}
		}

		crypt_password = crypt((char*)pass, pwd->pw_passwd);

		if ( ( strlen(pass) > 0 ) && ( crypt_password  == NULL || strcmp(crypt_password, pwd->pw_passwd) != 0 )) {
			fprintf(stderr,"Wrong password.\n");
			return -1;
		}
		free(crypt_password);
	}

	if( pcsc_init_ctx(reader_n, &PCSCctx) < 1 ) {
		return -1;
	}

	/* Generate id_crypt: sha512 hash of salted card ID */
	ID=pcsc_read_card_id(&PCSCctx);
	if(ID.uid_len == 0) {
		fprintf(stderr,"Card read failed\n");
		return -1;
	} else fprintf(stderr,"Card ID: %s\n",ID.UID);

	if(!RAND_bytes((unsigned char *)salt, 2)) fprintf(stderr,"Random salt generation failed."); ;
	id_salted=malloc(ID.uid_len+2);
	salt_ptr=id_salted+ID.uid_len;
	memcpy(id_salted, ID.uid_bytes,ID.uid_len);
	memcpy(salt_ptr, salt, 2);
	/*fprintf(stderr,"Salted Card ID: %s\n",bytes2hex(id_salted,ID.len+2)); */

	crypt_value=malloc(EVP_MAX_MD_SIZE);
	crypt_len = crypt_sha512(id_salted, ID.uid_len+2, crypt_value);
	crypt_value=realloc(crypt_value,crypt_len+2);
	salt_ptr=crypt_value+crypt_len;
	memcpy(salt_ptr, salt, 2);
	id_crypt=bytes2hex(crypt_value, crypt_len+2);
	crypt_len = crypt_sha512((char*)ID.uid_bytes, ID.uid_len, crypt_value);
	id_scrypt=bytes2hex(crypt_value, crypt_len);
	/* id_crypt ready */
	if ( NULL != pass && strlen(pass) > 0 ) {
		crypt_data.szKey=KEY_SIZE+ID.uid_len;
		crypt_data.Key=malloc(crypt_data.szKey);
		if(RAND_bytes((unsigned char*)crypt_data.Key, KEY_SIZE) != 1) {
			fprintf (stderr,"Random generator failed\n");
			return -3;
		}
		p_tmp=crypt_data.Key+KEY_SIZE;
		memcpy(p_tmp,ID.uid_bytes,ID.uid_len);

		crypt_data.In=pass;
		crypt_data.szIn=strlen(pass);
		if(!(n=pwencrypt(&crypt_data))) {
			fprintf(stderr,"Encryption failed code %d\n",n);
			return -1;
		}
		crypt_password=bytes2hex(crypt_data.Out, crypt_data.szOut);
		IV_s=bytes2hex(crypt_data.iv, IV_SIZE);
		key_s=bytes2hex(crypt_data.Key, KEY_SIZE);
		printf("%s:%s:%s:%s:%s%s\n",user,service_s,id_crypt,method,key_s,IV_s);

		/*crypt_password=realloc(crypt_password,strlen(crypt_password)+IV_SIZE+1);
		strcat(crypt_password,IV_s);*/
		printf("%s:%s\n",id_scrypt,crypt_password);
		free(crypt_password);
	} else if (strcmp(method,"id") == 0) {
		printf("%s:%s:%s:id\n",user,service_s,id_crypt);
	}

	free(pass);
	free(id_crypt);
	pcsc_free_ctx(&PCSCctx);

	return 0;
}
