#if ! defined(__sun)
#define _BSD_SOURCE     /* Get getpass() declaration from <unistd.h> */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE   /* Get crypt() declaration from <unistd.h> */
#endif
#endif

#include <unistd.h>
#include <limits.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <security/pam_modules.h>

#include "pcsc.h"
#include "pwcrypt.h"
#include "rfshadow.h"

#include "log.h"

int main(int argc, char *argv[])
{
	char option[256];

	PCSC_CONTEXT PCSCctx;
	CARD_ID cid;
	char dbfile[1024];
	char conf_file[1024];
	char amethod[64];
	char service[64];
	int n;

	char *user = NULL;
	char *password;
	VERBOSE=1;

	strcpy(dbfile,SHADOW_FILE);
	strcpy(amethod,"id");
	strcpy(service,"*");
	strcpy(conf_file,CONF_FILE);

	for(n=1;n<argc-1;n++)
	{
		strcpy(option,argv[n]);
		if(option[0] != '-') { continue; }
		switch(option[1]) {
			case 'r':
				READER_N=atoi(argv[n+1]);
				n++;
			break;
			case 'd':
				strcpy(dbfile,argv[n+1]);
				n++;
			break;
			case 'm':
				strcpy(amethod,argv[n+1]);
				n++;
			break;
			case 's':
				strcpy(service,argv[n+1]);
				n++;
			break;
			case 'c':
				strcpy(conf_file,argv[n+1]);
				n++;
			break;
		}
	}
	if ( (user=strdup(argv[argc-1])) == NULL)
	{
		fprintf(stderr,"Usage: %s [-r <reader num>] [-m <auth method>] [ -c <conf file> ]-d <db file> -s <pam service> user \n",argv[0]);
		return 255;
	}

	/* read config */

	if (!config_readfile(conf_file))
	{
		log_info("Error reading configuration file %s. Using defaults.\n",conf_file);
	}

	if(pcsc_init_ctx(READER_N, &PCSCctx)<1) {fprintf(stderr,"PCSC context init failed for reader#%d\n",READER_N); return -1;}
	cid=pcsc_read_card_id(&PCSCctx);
	if(cid.uid_len == 0) {fprintf(stderr,"Card read failed\n"); return -1;}
	pcsc_free_ctx( &PCSCctx);

	printf("card: %s\n",bytes2hex((char *)cid.uid_bytes,cid.uid_len));

	/*ptr_file =fopen(dbfile, "r");
	if (!ptr_file) {
		log_error("Can not open file %s \n",dbfile);
		return -1;
	}*/
	if(strcmp(amethod,"id") != 0)
	{
		if((n=pcsc_auth_user(READER_N,user,service,&password)) == PAM_SUCCESS )
		{
			printf("Password: %s \n",password);
		}
	} else {
		n=rfid_auth_user(READER_N,user,service);
	}
	if(n != PAM_SUCCESS )
	{
		printf(">> user: %s service: %s failed with method %s. \n",user,service,amethod);
	}

	
	return  0;
}
