#if ! defined(__sun)
#define _BSD_SOURCE     /* Get getpass() declaration from <unistd.h> */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE   /* Get crypt() declaration from <unistd.h> */
#endif
#endif

#include <unistd.h>
#include <limits.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <malloc.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <shadow.h>
#include <errno.h>
#include <security/pam_modules.h>

#include "config.h"
#include "pwcrypt.h"
#include "rfshadow.h"
#include "log.h"

FILE* rfshadow_openfile(const char* user)
{
	FILE* ptr_file;
	struct stat sb;
	struct passwd *pw;
	uid_t		uid,euid;
	char *id_file;

	ptr_file=NULL;
	euid=geteuid();
	uid=getuid();
	log_debug("Call open file by uid=%d euid=%d \n",uid, euid);

	pw = getpwnam(user);
	if(pw == NULL) { log_error("Unknown user %s in pam_pcsc!!! Really wrong PAM configuration!\n",user); return NULL; }
	/*    if ( != 0) {
         return PAM_CRED_INSUFFICIENT;
       }*/
/*
The return value may point to a static area, and may be overwritten by subsequent calls to getpwent(3), getpwnam(), or getpwuid(). 
Do not pass the returned pointer to free().
*/
	if(euid == 0 ) { /* We can read global file */
		id_file=SHADOW_FILE;
		ptr_file =fopen(id_file, "r");
		if (!ptr_file) log_error("Can not open global shadow file %s \n",user,id_file);
		else {
				if (stat(id_file, &sb) == -1) {
					log_error("Can not stat file %s \n",id_file);
					fclose(ptr_file);
					ptr_file=NULL;
				} else {
					if(!S_ISREG ( sb.st_mode ) || (sb.st_mode & (S_IWGRP | S_IRWXO)) ) {
						log_error("Wrong permissions for global file %s. Skipping.\n",id_file);
						fclose(ptr_file);
						ptr_file=NULL;
					}
				}
		}
	}

	if(pw->pw_uid == uid && ptr_file == NULL) { /* let's try user's home directory file */
	/* but only if the user matches process owner ID */
			id_file = pw->pw_dir;
			strcat(id_file,"/");
			strcat(id_file,USER_ID_FILE);
			log_debug("Not root, open user's file %s\n",id_file);
			ptr_file =fopen(id_file, "r");
			if (stat(id_file, &sb) == -1) {
				log_error("Can not stat file %s \n",id_file);
				fclose(ptr_file);
				ptr_file=NULL;
			} else {
				if(!S_ISREG ( sb.st_mode ) || (sb.st_mode & (S_IRWXG | S_IRWXO)) ) {
					log_error("Wrong permissions for user's file %s. Skipping.\n",id_file);
					fclose(ptr_file);
					ptr_file=NULL;
				}
			}
	} else if (ptr_file == NULL) log_debug("Request from UID=%d to UID=%d will not work. Sorry. \n",uid,pw->pw_uid);

	return ptr_file;
}

size_t rfshadow_getline (FILE*ptr_file, const char*user, const char*service, const CARD_ID ID, AUTH_STRUCT* auth)
{
    char line[LINE_BUFFER_LEN];
    char *sep=FIELD_SEPARATOR;
    char *element;
	char* element_bytes;
	unsigned int bytes;

	char *crypt_card_id;
	int crypt_len;
	char *salted_card_id;
	char *salt_ptr;

	int ln=0;
	int line_found=0;

while (fgets(line,sizeof(line),ptr_file)!=NULL)
{
	ln++;
	log_debug("[%d]%s$=%d\n",ln,line,strlen(line));
	if((line[0] == '#') || (strlen(line)<2) ) continue;
	if (line[strlen(line) - 1] == '\n') {
	line[strlen(line) - 1] = '\0';
	}
	if ( (element=strtok(line,sep)) != NULL){ /* USER */
		if(strcmp(element,user)) continue;
	} else { log_debug("Invalid shadow line format. Missing field : user.","\n"); continue; }

	if ( (element=strtok(NULL,sep)) != NULL) { /* SERVICE */
		if((strcmp(element,service)!=0)&&(strcmp(element,"*")!=0)) continue;
	} else { log_debug("Invalid file format. Missing field : service.","\n"); continue; }


	if ((element=strtok(NULL,sep)) != NULL){ /* SALTED ID HASH */

	salted_card_id=malloc(ID.uid_len+2);
	memcpy(salted_card_id, ID.uid_bytes,ID.uid_len);
	salt_ptr=salted_card_id+ID.uid_len;

	/* get salt */
	bytes=hex2bytes(element,&element_bytes);
	if(bytes <= 0) { log_debug("hex2bytes failed for %s. \n",element); continue;}

	log_debug("Hash card read %s of %d bytes. \n",element,bytes);	
	
	memcpy(salt_ptr, element_bytes+bytes-2,2);

	log_debug("Salted card ID %s. \n",bytes2hex(salted_card_id,ID.uid_len+2));

	crypt_card_id=malloc(EVP_MAX_MD_SIZE);
	crypt_len = crypt_sha512(salted_card_id,ID.uid_len+2, crypt_card_id);

	/*log_debug("Hashed card ID %s. \n",bytes2hex(crypt_card_id,crypt_len)); */

	free(salted_card_id);
	
	if(memcmp(element_bytes,crypt_card_id,crypt_len) != 0) {
		free(crypt_card_id);
		free(element_bytes);
		continue;
	}
	free(crypt_card_id);
	free(element_bytes);

	} else { log_debug("Invalid shadow line format: card ID hash not found.","\n"); continue; }
	/* Selection end -----------------------



	Reading data */
	if (! line_found )
	{
		if ( (element=strtok(NULL,sep)) != NULL) { /* METHOD */
		strncpy(auth->method,element,7);
		} else { log_debug("Invalid shadow line format. Missing field : method. Line %d \n",ln); continue; }
		line_found=1;
		/* Line was found. Reading crypto data. /*/
		if ((element=strtok(NULL,sep)) != NULL){ /* DATA */
			auth->data=malloc((strlen(element)+1)*sizeof(char));
			strcpy(auth->data,element);
			log_debug("Auth Data : %s\n",auth->data);
		} else { log_debug("No data found in line #%d. \n",auth->method,ln);}
	} else {
		if ( (element=strtok(NULL,sep)) != NULL) {
			log_info("Extra line #%d for user/service : %s/%s. Method : '%s' will be ignored\n",ln,user,service,element);
		} else { log_debug("Invalid shadow line format at line %d. Missing field : method. \n",ln); continue; }
	}
}

return line_found;

}

char* idp_getline (const CARD_ID ID)
{
    FILE*ptr_file;
	char line[LINE_BUFFER_LEN];
    char *sep=FIELD_SEPARATOR;
	char *data=NULL;
    char *element;
	char* element_bytes;
	unsigned int bytes;

	char *crypt_card_id;
	int crypt_len;
	char *salted_card_id;
	char *salt_ptr;

	int ln=0;
	int line_found=0;

	if(IDP_FILE != NULL) {
		ptr_file =fopen(IDP_FILE, "r");
	} else {
		log_debug("IDP data file not configured \n",IDP_FILE);
		return NULL;
	}
	if (!ptr_file) {
		log_error("Can not open data file %s \n",IDP_FILE);
		return NULL;
	}

while (fgets(line,sizeof(line),ptr_file)!=NULL)
{
	ln++;
	log_debug("%s\n",line);
	if((line[0] == '#') || (strlen(line)<2) ) continue;
	if (line[strlen(line) - 1] == '\n') {
	line[strlen(line) - 1] = '\0';
	}
	if ((element=strtok(NULL,sep)) != NULL){ /* SALTED ID HASH */

		salted_card_id=malloc(ID.uid_len+2);
		memcpy(salted_card_id, ID.uid_bytes,ID.uid_len);
		salt_ptr=salted_card_id+ID.uid_len;

		/* get salt */
		bytes=hex2bytes(element,&element_bytes);
		if(bytes <= 0) { log_debug("hex2bytes failed for %s. \n",element); continue;}

		log_debug("Hash card read %s of %d bytes. \n",element,bytes);

		memcpy(salt_ptr, element_bytes+bytes-2,2);

		log_debug("Salted card ID %s. \n",bytes2hex(salted_card_id,ID.uid_len+2));

		crypt_card_id=malloc(EVP_MAX_MD_SIZE);
		crypt_len = crypt_sha512(salted_card_id,ID.uid_len+2, crypt_card_id);

		/*log_debug("Hashed card ID %s. \n",bytes2hex(crypt_card_id,crypt_len)); */

		free(salted_card_id);

		if(memcmp(element_bytes,crypt_card_id,crypt_len) != 0) {
			free(crypt_card_id);
			free(element_bytes);
			continue; /* Next loop is crypts don't match */
		}
		free(crypt_card_id);
		free(element_bytes);

	} else { log_debug("Invalid shadow line format: card ID hash not found.","\n"); continue; }
	/* Selection end -----------------------



	Reading data */
	if (! line_found )
	{
		if ((element=strtok(NULL,sep)) != NULL){ /* DATA */
			data=malloc((strlen(element)+1)*sizeof(char));
			strcpy(data,element);
			log_debug("Auth Data : %s . \n",data);
		} else { log_debug("Auth data not found for this card: line %d","\n",ln); continue; }
		line_found=1;
		/* log_debug("DEBUG: %s|%s\n",user,auth->data); */
	} else {
		log_info("Extra line for this card will be ignored: line %d","\n",ln);
	}
}

return data;

}

