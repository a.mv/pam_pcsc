#include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/pem.h>

#define IV_SIZE 8
#define KEY_SIZE 128
#define CRYPT_BUFFER_SIZE 4096

typedef struct crypt_struct
{
	char iv[IV_SIZE];
	char * Key; 
	size_t szKey;
	char * In;
	size_t szIn;
	char Out[CRYPT_BUFFER_SIZE];
	size_t szOut;
} CRYPT_STRUCT;

int genkey(const char *message, unsigned int len, char * KEY);
int crypt_sha512(const char *message, unsigned int len, char * value);
unsigned int hex2bytes(const char * hex, char ** bytes);
char *  bytes2hex(const char * bytes, unsigned int bytesize);
int pwencrypt (CRYPT_STRUCT* crypt);
int pwdecrypt (CRYPT_STRUCT* crypt);
void free_crypt(CRYPT_STRUCT c);