pam_pcsc
========

pam_pcsc is a NFC-based PAM authentification module.

Using pam_pcsc, you can use NFC (Near Field Communication) cards / tags to
autohrise users into PAM (Pluggable Authentication Modules).


How does it work?
-------------

### Basics

It uses *pscs-lite* framework (http://ludovic.rousseau.free.fr/softwares/) to communicate with the reader so should work with any reader supported by the framework.

It was tested with Mifare Ultralight cards, but should work with any memory card that your reader can read and write.

First of all you register your card for authentication with ` pam_registercard.pl <user> ` as _root_. 
The program will asks you the password which will be checked by usual systemwide password check.

Then it will detect your card, encrypt the password, write it to the card and check that it is readable.

It will print you a line to put to **/etc/rfshadow** file. This line contains the encryption key for your password, stored on the card.

In order to authenticate with cards you need to put this module to your PAM configuration files in /etc/pam.d:

` auth    optional        pam_pcsc.so `

somewere before this one:

` auth      required  pam_unix.so     try_first_pass nullok `

Be sure to put the **pam_rfid** line before the **pam_unix** in order to avoid standard password prompt.

When the module is sollicitated during the PAM authentication process it will read the card search a key in **rfshadow** file 
and try to decrypt the password. In success the password will be sent down to the PAM stack like if you typed it. If not, PAM will just continue
and give you a standard password prompt.

The module checks that the authentication request is from local host in order to avoid compromising remote security.

### Password encryption

For encrypting the password a RSA key is generated and stored in the **rfshadow** file. This key is random and unique for each card. 

Build and install
-----------------

Dependencies:

-   pcsc-lite

-   openssl

-   libconfig (from http://www.hyperrealm.com/libconfig/)

-   Libconfig.pm (http://search.cpan.org/~cnangel/Conf-Libconfig-0.05/lib/Conf/Libconfig.pm)

Perl module for PCSC is included into pcsc-lite.

Build command:
` autoreconf -vfi ` 

Debian packaging files are also present. You can build a debian package with

` debuild -i -us -uc -b `

Usage
-----

Place your card to the reader before to use anything (enroll or authentication). The code don't wait for a card.

### To register a card

Put your card to the reader and launch:

` pam_registercard.pl <user> `

the program will ask for a password, write it encrypted to the card then output a line you should copy and paste to **/etc/rfshadow** file. 
**pam_registercard.pl** tool is not sufficiently tested for allowing it to write this quite important file.
This file must be owned by root and must have at most 640 permission.
In this line: 

* user
* service - PAM service this card authenticates. By default **pam_registercard.pl** put "*" in it, which means any service. You can restrict this card to one service, for example _su_.
* hashed card ID
* method - currently the only supported method by **pam_pcsc** is _mc_. _id_ method is for **pam_rfid** module which will just match _user_ with _Card ID_ and return PAM_SUCCESS. In current version it is not tested at all. No password will be stored with this method.
* authentication data - for _mc_ it is encryption key, and empty for _id_

File shoud contain only one line for user/service/ID combination. If not, only first line will be used and a message will be logged.



It will also write card detection information to **/var/pam_pcsc/card.red**. It contains total number of writable blocks, written blocks and size of encrypted password.
PAM module need this infomation in order correctly read the card.

### Authentication

Put the card to the reader before login. PAM Module will not wait for a card.

### Configuration

Card detection uses **/usr/share/pam_pcsc/mc.def** file to identify the card. It contains regexes for known card's ATR and some useful information about card structure.
Tools and modules use **/etc/pcsc.conf** file for Card reader configuration. Configuration includes the reader's number. If you have more than one reader on the system you should put the number of the reader you will use. **pcsc_scan** tool from *pscs-lite* package is useful for understanding your configuration and cards and correctly adjust this file.

The configuration file also contains APDUs for reading and writing cards. By default there are standard APDUs but it could be different for your reader. A good tool for checking APDUs is **gscriptor** from *pscs-lite*.

To do
-----

1. **pam_registercard.pl** is a real mess. Need to clean up the code and implement all features: write to rfshadow, keep lines unique, remove lines, update lines and password.
1. Protect used blocks on the card by internal key.

