#if ! defined(__sun)
#define _BSD_SOURCE     /* Get getpass() declaration from <unistd.h> */
#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE   /* Get crypt() declaration from <unistd.h> */
#endif
#endif

#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <termios.h>
#include <pwd.h>
#include <shadow.h>
#include <errno.h>
#include <security/pam_modules.h>

#include "log.h"
#include "pwcrypt.h"
#include "pcsc.h"
#include "pcsc_auth.h"
#include "rfshadow.h"


int pcsc_auth_user (const int reader_n, const char * user,const char * service, char ** password)
{
int ret=PAM_SYSTEM_ERR;
#define Exit(a) ret=a; goto EXIT

	size_t ln;
	FILE *ptr_file;

	PCSC_CONTEXT PCSCctx;
	CARD_ID cid;

	AUTH_STRUCT auth_line;
	char *idp_line;
	CRYPT_STRUCT	crypt;

	/* Decryption memory buffers */

	char * data_ptr;
	char *iv;
	unsigned int iv_len;
	unsigned int iv_start;
	crypt.szKey=0;
	crypt.szIn=0;
	crypt.szOut=0;
	auth_line.data=NULL;

	/* Connecting card, read card ID*/

	log_debug("RF: Reading card ID \n",NULL);
	if(!pcsc_init_ctx(reader_n, &PCSCctx)) return PAM_AUTHINFO_UNAVAIL;
	cid=pcsc_read_card_id(&PCSCctx);
	if(cid.uid_len == 0) { Exit(PAM_CRED_UNAVAIL); }
	log_debug("Card ID: %s\n",cid.UID);

	/* Read auth file*/
	ptr_file=rfshadow_openfile(user);
	if (ptr_file==NULL) {	Exit(PAM_AUTHINFO_UNAVAIL); }
	ln=rfshadow_getline(ptr_file,user,service,cid, &auth_line);
	fclose(ptr_file);
	
	if(! ln) {
		log_error("User \"%s\" is not registred for \"%s\" with this card.\n",user,service);
		Exit(PAM_USER_UNKNOWN);
	}
	if(auth_line.data == NULL) {
		log_error("Auth data is empty for user \"%s\", service \"%s\".\n",user,service);
		Exit(PAM_AUTHINFO_UNAVAIL);
	}
	/* Parsing auth data for crypt info */
	iv_start=strlen(auth_line.data)-IV_SIZE*2*sizeof(char); /* IV */
	data_ptr=(char*)(auth_line.data+iv_start);
	iv_len = hex2bytes(data_ptr,&iv);
	auth_line.data[iv_start]='\0';
	crypt.szKey = hex2bytes(auth_line.data,&crypt.Key);
	if( (0 == crypt.szKey) || (iv_len != IV_SIZE) ) {
		log_error("Invalid auth data for user/service/method %s/%s/%s\n",user,service,auth_line.method);
		Exit(PAM_AUTHINFO_UNAVAIL);
	} else {
		memcpy(crypt.iv,iv,iv_len);
		log_debug("IV: %s(%d)\nKEY: %s\n",bytes2hex(crypt.iv,IV_SIZE),iv_len,bytes2hex(crypt.Key,crypt.szKey));
		crypt.Key=(char*)realloc(crypt.Key,crypt.szKey+cid.uid_len);
		data_ptr=(char*)(crypt.Key+crypt.szKey*sizeof(char));
		memcpy(data_ptr,cid.uid_bytes,cid.uid_len);
		crypt.szKey+=cid.uid_len;
	}

	crypt.szIn=0;
	log_debug("Continue with method %s\n",auth_line.method);
	if(strcmp(auth_line.method,"idp") == 0)
	{
		/* Card ID auth with password storage emulation*/
		idp_line=idp_getline(cid);
		if(strlen(idp_line)>0){
			crypt.szIn=hex2bytes(idp_line,&crypt.In);
		}
		free(idp_line);
	} else if(strcmp(auth_line.method,"mc") == 0)  {
		/* Memory Card read */
		if(! pcsc_read_registry(&cid)){
			log_error("Unregistred card %s\n",cid.UID);
			Exit(PAM_CRED_UNAVAIL);
		}
		if(! pcsc_read_data(&PCSCctx,cid,&crypt.In,&crypt.szIn) ){
			log_error("Card read error %s\n",cid.UID);
			Exit(PAM_CRED_UNAVAIL);
		}
	}

	if(crypt.szIn == 0){
		log_debug("Method: %s . Card data is empty. Card ID: %s\n",auth_line.method,bytes2hex((char*)cid.uid_bytes, cid.uid_len));
		Exit(PAM_CRED_UNAVAIL);
	}
	log_debug("Card data [%d]: %s\n",crypt.szIn,bytes2hex((char*)crypt.In, crypt.szIn));
	ret=pwdecrypt(&crypt);
	if( ret == 0 ) { log_debug("Wrong paramters for Password decryption.\n",NULL); }
	if( ret <= 0 ) { log_error("Password decryption failed for user \"%s\" \n",user); Exit(PAM_CRED_UNAVAIL); }
	else
	{
		crypt.Out[crypt.szOut]='\0';
		*password=strdup(crypt.Out);
	}
	ret=PAM_SUCCESS;
	log_info("User \"%s\" is authenticated by RF card.\n",user);
EXIT:
	if(auth_line.data!=NULL) { memset(auth_line.data,'\0',strlen(auth_line.data)); free(auth_line.data); }
	pcsc_free_ctx(&PCSCctx);
	free_crypt(crypt);
	return ret;

}

int rfid_auth_user (const int reader_n, const char * user,const char * service)
{
	FILE* ptr_file;
    size_t ln;

	PCSC_CONTEXT PCSCctx;
	CARD_ID cid;

	AUTH_STRUCT auth_line;

	int ret=PAM_SYSTEM_ERR;
#define Exit(a) ret=a; goto EXIT

	/* Connecting card, read card ID*/
	auth_line.data=NULL;

	log_debug("RF: Reading card ID \n",NULL);
	if(!pcsc_init_ctx(reader_n, &PCSCctx)) return PAM_AUTHINFO_UNAVAIL;
	cid=pcsc_read_card_id(&PCSCctx);
	if(cid.uid_len == 0) { Exit(PAM_CRED_UNAVAIL); }
	log_debug("Card ID: %s\n",cid.UID);

	/* Read auth file*/
	if (!(ptr_file=rfshadow_openfile(user))) {	Exit(PAM_AUTHINFO_UNAVAIL); }
	ln=rfshadow_getline(ptr_file,user,service,cid, &auth_line);
	fclose(ptr_file);

	if(! ln) {
		log_error("User \"%s\" is not registred for \"%s\" with this card.\n",user,service);
		Exit(PAM_USER_UNKNOWN);
	}

	if(strcmp(auth_line.method,"id") != 0)
	{
		Exit(PAM_USER_UNKNOWN);
	}

	ret=PAM_SUCCESS;
	log_info("User \"%s\" is authenticated by RFID card.\n",user);
EXIT:
	if(auth_line.data!=NULL) { memset(auth_line.data,'\0',strlen(auth_line.data)); free(auth_line.data); }
	pcsc_free_ctx(&PCSCctx);
	return ret;

}