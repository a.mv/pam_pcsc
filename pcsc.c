#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "pcsc.h"
#include "config.h"
#include "log.h"
#include "pwcrypt.h"


#define CHECK(f, rv) \
 if (SCARD_S_SUCCESS != rv) \
 { \
  log_debug(f ": %s\n", pcsc_stringify_error(rv)); \
  goto FAIL; \
 }

#define LOG(f, rv) \
 if (SCARD_S_SUCCESS != rv) \
 { \
  log_debug(f ": %s\n", pcsc_stringify_error(rv)); \
 }


BYTE pbRecvBuffer[PCSC_BUFFER_LEN];


int pcsc_init_ctx(int reader_n, PCSC_CONTEXT * pcsc)
{
	DWORD dwActiveProtocol;
	LONG rv;

	LPTSTR mszReaders;
	LPTSTR Reader;
	DWORD dwReaders;


	unsigned int i;

	rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &pcsc->hContext);CHECK("SCardEstablishContext", rv)

	#ifdef SCARD_AUTOALLOCATE
	dwReaders = SCARD_AUTOALLOCATE;

	rv = SCardListReaders(pcsc->hContext, NULL, (LPTSTR)&mszReaders, &dwReaders);CHECK("SCardListReaders", rv)
	#else
	rv = SCardListReaders(pcsc->hContext, NULL, NULL, &dwReaders);CHECK("SCardListReaders", rv)

	mszReaders = calloc(dwReaders, sizeof(char));
	rv = SCardListReaders(pcsc->hContext, NULL, mszReaders, &dwReaders);CHECK("SCardListReaders", rv)
	#endif

	Reader=mszReaders;
	for(i=1;i<=reader_n;i++)
	{
		Reader=strchr(mszReaders,'\0')+1;
	}
	strcpy(pcsc->Reader,Reader);

	log_info("PCSC reader : %s\n",pcsc->Reader);

	rv = SCardConnect(pcsc->hContext, pcsc->Reader, SCARD_SHARE_SHARED,
	SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &pcsc->hCard, &dwActiveProtocol);CHECK("SCardConnect", rv)

	 switch(dwActiveProtocol)
	 {
	  case SCARD_PROTOCOL_T0:
	   pcsc->pioSendPci = *SCARD_PCI_T0;
	   break;

	  case SCARD_PROTOCOL_T1:
	   pcsc->pioSendPci = *SCARD_PCI_T1;
	   break;
	 }


	return 1;
FAIL:
	return 0;
}

int pcsc_free_ctx(PCSC_CONTEXT * pcsc)
{
	LONG rv;
	rv = SCardDisconnect(pcsc->hCard, SCARD_LEAVE_CARD);LOG("SCardDisconnect", rv)
	rv = SCardReleaseContext(pcsc->hContext);LOG("SCardReleaseContext", rv)

	return 1;
}

size_t pcsc_read_card(PCSC_CONTEXT * pcsc,const char * apdu_s, BYTE ** read_data)
{
	DWORD dwRecvLength;
	LONG rv;
	char *apdu;
	unsigned int apdu_size;

	dwRecvLength = sizeof(pbRecvBuffer);
	if(dwRecvLength <= 0) {
		log_debug("Zero size pbRecvBuffer : in  pcsc_read_card","\n");
		return 0;
	}

	apdu_size=hex2bytes(apdu_s, &apdu);
	if(apdu_size < 1) return 0;

	log_debug("Send APDU: %s\n",apdu_s);

	rv = SCardTransmit(pcsc->hCard, &pcsc->pioSendPci, (LPCBYTE) apdu, apdu_size,
	  NULL, pbRecvBuffer, &dwRecvLength);CHECK("SCardTransmit", rv)

	if(dwRecvLength <= 2) return 0;

	*read_data=malloc(sizeof(BYTE)*dwRecvLength);
	if(memcpy(*read_data,pbRecvBuffer,dwRecvLength) == NULL)
	{
		memset(pbRecvBuffer,'\0',dwRecvLength);
		log_debug("memcpy failed !! : in  pcsc_read_card","\n");
		return -1;
	}
	memset(pbRecvBuffer,'\0',dwRecvLength);

	return dwRecvLength-2;
FAIL:
	return 0;
}

CARD_ID pcsc_read_card_id(PCSC_CONTEXT * pcsc)
{
	CARD_ID id;

/* ATR reading block */
/*
	SCARD_READERSTATE rgReaderStates[1];
	LONG rv;
	DWORD timeout=1000;
	id.uid_len=0;

	rgReaderStates[0].szReader = pcsc->Reader;
	rgReaderStates[0].dwCurrentState = SCARD_STATE_UNAWARE;
	rgReaderStates[0].cbAtr = sizeof rgReaderStates[0].rgbAtr;

	rv = SCardGetStatusChange(pcsc->hContext, timeout, rgReaderStates, 1);LOG("SCardGetStatusChange", rv)
	if (rgReaderStates[0].dwEventState & SCARD_STATE_CHANGED)
	{
	    if (rgReaderStates[0].cbAtr > 0)
	    {
			id.atr_len=sizeof(BYTE)*rgReaderStates[0].cbAtr;
			id.atr_bytes=malloc(id.atr_len);
			memcpy(id.atr_bytes,rgReaderStates[0].rgbAtr,id.atr_len);
	    }
	}
*/
	id.uid_len=pcsc_read_card(pcsc,APDU_GET_ID,&id.uid_bytes);
	id.UID=bytes2hex((char*)id.uid_bytes, id.uid_len);
	return id;
}

void free_card_id(CARD_ID id){
	if(NULL != id.uid_bytes) free(id.uid_bytes);
	if(NULL != id.UID)free(id.UID);
	if(NULL != id.atr_bytes) free(id.atr_bytes);
	if(NULL != id.Blocks)free(id.Blocks);
}

int pcsc_read_data(PCSC_CONTEXT * pcsc,CARD_ID id, char ** data, size_t* size) {
	char command[256];
	char block[3];
	int i;
	int to_read=0;

	BYTE *cread;
	size_t bytes_read;

	block[2]='\0';
	*size=0;
	*data=NULL;
	to_read=id.data_size;

	for(i=0;i<strlen(id.Blocks);i+=2){
		strncpy(block,(char*)(id.Blocks+i*sizeof(char)),2);
		if(sprintf(command,APDU_READ_BLOCK,block)>0) bytes_read=pcsc_read_card(pcsc,command,&cread);
		else return 0;
		if(bytes_read <=0) return 0;

		to_read-=id.block_size;
		if(to_read > 0 && to_read < id.block_size) bytes_read=to_read;
		else bytes_read=id.block_size;
		*data=(char*)realloc(*data,*size+bytes_read);
		memcpy((char*)(*data+*size),cread,bytes_read);
		*size+=bytes_read;
		memset(cread,'\0',bytes_read);free(cread);
		/* log_debug("Readed: %s\n",bytes2hex(*data,*size)); */
	}
	return 1;
}
int pcsc_read_registry(CARD_ID * id){
	FILE* ptr_file=NULL;
    char line[LINE_BUFFER_LEN];
    char *sep=FIELD_SEPARATOR;
	char *id_scrypt;
    char *element;
	char *element_bytes;
	int len;
	int success=0;

	if(REG_FILE != NULL) {
		ptr_file =fopen(REG_FILE, "r");
	} else {
		log_error("No registry file defined \n",REG_FILE);
		return 0;
	}
	if (!ptr_file) {
		log_error("Can not open registry file %s \n",REG_FILE);
		return 0;
	}

	/* calculating Card's ID hash and storing in HEX format */
	element_bytes=malloc(EVP_MAX_MD_SIZE);
	len = crypt_sha512((char*)id->uid_bytes, id->uid_len, element_bytes);
	id_scrypt=bytes2hex(element_bytes, len);
	free(element_bytes);

	while (fgets(line,sizeof(line),ptr_file)!=NULL)
	{
		log_debug("%s\n",line);
		if((line[0] == '#') || (strlen(line)<2) ) continue;
		if (line[strlen(line) - 1] == '\n') {
			line[strlen(line) - 1] = '\0';
		}
		if ( (element=strtok(line,sep)) != NULL ){ /* ID HASH */
			if(strcmp(element,id_scrypt)) continue;
		} else { log_debug("Invalid registry line format. Missing field : ID HASH.\n",NULL); continue; }

		/* CARD LINE FOUND */
		success=0;
		if ( (element=strtok(NULL,sep)) != NULL ){ /* block size */
			id->block_size=atoi(element);
			log_debug("block_size=%d\n",id->block_size);
		} else { log_debug("Invalid registry line format. Missing field : block size\n",NULL); continue; }
		if ( (element=strtok(NULL,sep)) != NULL ){ /* used blocks */
			/*id.nbBlocks=hex2bytes(element,&id.Blocks);*/
			id->Blocks=strdup(element);
			log_debug("Used blocks=%s\n",id->Blocks);
		} else { log_debug("Invalid registry line format. Missing field : used block.\n",NULL); continue; }
		if ( (element=strtok(NULL,sep)) != NULL ){ /* data size */
			id->data_size=atoi(element);
			log_debug("data_size=%d\n",id->data_size);
		} else { log_debug("Invalid registry line format. Missing field : data size.\n",NULL); continue; }
		success=1;
	}
	return success;
}