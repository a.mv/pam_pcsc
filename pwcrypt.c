#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pwcrypt.h"



int genkey(const char *message, unsigned int len, char * KEY)
{
 EVP_MD_CTX *mdctx;
 const EVP_MD *md;

 char *INIT;
 char *it; 

 int i=100;
 
 unsigned int md_len;

 OpenSSL_add_all_digests();
 md = EVP_get_digestbyname("sha256");
 if(!md) {
        return 0;
 }

 INIT=malloc(len*2*sizeof(char));
 memcpy(INIT,message,len/2);
 it=INIT+len/2;
 memcpy(it,message,len);
 it=it+len;
 memcpy(it,message+len/2,len/2+len%2);
 len=len*2;

 mdctx = EVP_MD_CTX_create();
 EVP_DigestInit_ex(mdctx, md, NULL);
 EVP_DigestUpdate(mdctx, INIT, len);
 EVP_DigestFinal_ex(mdctx, (unsigned char*)KEY, &md_len);


 for(;i>0;i--)
 {
	 EVP_DigestInit_ex(mdctx, md, NULL);
	 EVP_DigestUpdate(mdctx, (unsigned char*)KEY, md_len);
	 EVP_DigestFinal_ex(mdctx, (unsigned char*)KEY, &md_len);
 }
 EVP_MD_CTX_destroy(mdctx);

 free(INIT);
 return md_len;
}

int crypt_sha512(const char *message, unsigned int len, char * value)
{
 EVP_MD_CTX *mdctx;
 const EVP_MD *md;
 
 unsigned int md_len;

 OpenSSL_add_all_digests();
 md = EVP_get_digestbyname("sha512");
 if(!md) {
        return 0;
 }
 mdctx = EVP_MD_CTX_create();
 EVP_DigestInit_ex(mdctx, md, NULL);
 EVP_DigestUpdate(mdctx, message, len);
 EVP_DigestFinal_ex(mdctx, (unsigned char*)value, &md_len);

 EVP_MD_CTX_destroy(mdctx);

 return md_len;
}

unsigned int hex2bytes(const char * hex, char * * bytes)
{
const char *p;
unsigned int i;

char hexbyte[3];
unsigned long num;
char * buf;
unsigned int bytesize;
int byte;

hexbyte[2]='\0';

if(hex == NULL) return 0;
bytesize=strlen(hex)/2;
if(bytesize == 0) return 0;

        if(strlen(hex)%2 == 0)
        {
		buf=(char *)malloc(bytesize);
		for (i=0,p=hex;i<bytesize;i++,p+=2){
		        strncpy(hexbyte,p,2);
		        num=strtoul(hexbyte, NULL, 16);
			byte = (char)(num & 0xFF) ;
			buf[i]=byte;
		}
		* bytes=malloc(bytesize);
		memmove (* bytes,buf,bytesize);
		free(buf);
		
        } else {
                return 0;
        }
        return bytesize;
}


char *  bytes2hex(const char * bytes, unsigned int bytesize)
{
	unsigned int i;
	char byte[3];
	char * hex;
	unsigned int size;
	size=sizeof(unsigned char)*2*bytesize+1;
	hex=malloc(size);
	hex[0]='\0';
	for (i=0;i<bytesize;i++)
	{
		sprintf(byte,"%02x", (unsigned char)bytes[i]);
		strcat(hex,byte);
	}
	hex[size-1]='\0';
	return hex;
}

int
pwdecrypt (CRYPT_STRUCT* crypt)
{
	int tlen,rklen;
	char * real_key;
	EVP_CIPHER_CTX ctx;
	
	real_key=malloc(EVP_MAX_MD_SIZE);
	rklen=genkey(crypt->Key,crypt->szKey, real_key);
	if((rklen == 0) || (real_key == NULL)) return 0;

	EVP_CIPHER_CTX_init (&ctx);
	
	EVP_DecryptInit_ex (&ctx, EVP_bf_cbc(),NULL,NULL,NULL);

	EVP_CIPHER_CTX_set_key_length(&ctx, rklen);
	
	EVP_DecryptInit_ex (&ctx, NULL,NULL, (unsigned char*)real_key,(unsigned char*)crypt->iv);

		  if (EVP_DecryptUpdate (&ctx, (unsigned char*)crypt->Out, &tlen, (unsigned char*)crypt->In, crypt->szIn) != 1)
		    {
			    fprintf (stderr,"error in decrypt update\n");
			    return -3;
		    }
		crypt->szOut = tlen;
		  if (EVP_DecryptFinal (&ctx, (unsigned char*)(crypt->Out+crypt->szOut), &tlen) != 1)
		    {
			    fprintf (stderr,"error in decrypt final\n");
			    return -3;
		    }
		crypt->szOut += tlen;

	EVP_CIPHER_CTX_cleanup (&ctx);
	return 1;
}

int
pwencrypt (CRYPT_STRUCT* crypt)
{
	int tlen,rklen;
	char * real_key;
	EVP_CIPHER_CTX ctx;

	/*
	FILE *fd;
	if (!(fd = fopen ("/dev/random", "r")))
		return -1 ;
	if (!(fread (&iv, IV_SIZE,1,fd)))
		return -2;
	fclose (fd);
	*/
	if(RAND_bytes((unsigned char*)crypt->iv, IV_SIZE) != 1) 		    
	{
	    fprintf (stderr,"Random generator failed\n");
	    return -3;
    }
	real_key=malloc(EVP_MAX_MD_SIZE);
	rklen=genkey(crypt->Key,crypt->szKey, real_key);
	if((rklen == 0) || (real_key == NULL)) return 0;

	EVP_CIPHER_CTX_init (&ctx);

	EVP_EncryptInit_ex (&ctx, EVP_bf_cbc(),NULL,NULL,NULL);

	EVP_CIPHER_CTX_set_key_length(&ctx, rklen);

	tlen=EVP_CIPHER_CTX_key_length(&ctx);

	EVP_EncryptInit_ex (&ctx, NULL,NULL, (unsigned char*)real_key,(unsigned char*)crypt->iv);


		  if (EVP_EncryptUpdate (&ctx, (unsigned char*)crypt->Out,&tlen, (unsigned char*)crypt->In, crypt->szIn) != 1)
		    {
			    fprintf (stderr,"error in encrypt update\n");
			    return -3;
		    }
		crypt->szOut = tlen;
		  if (EVP_EncryptFinal (&ctx, (unsigned char*)crypt->Out+crypt->szOut, &tlen) != 1)
		    {
			    fprintf (stderr,"error in encrypt final\n");
			    return -3;
		    }
		crypt->szOut += tlen;

	EVP_CIPHER_CTX_cleanup (&ctx);
	return 1;
}

void free_crypt(CRYPT_STRUCT c){
	if(c.szKey > 0) { memset(c.Key,'\0',c.szKey); free(c.Key); c.szKey=0; }
	if(c.szIn > 0) { memset(c.In,'\0',c.szIn); free(c.In); c.szIn=0; }
	if(c.szOut > 0) { memset(c.Out,'\0',c.szOut); c.szOut=0; }
}

/* A BASE-64 ENCODER AND DECODER USING OPENSSL */
/*
char *base64encode (const void *b64_encode_this, int encode_this_many_bytes){
    BIO *b64_bio, *mem_bio;      //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    BUF_MEM *mem_bio_mem_ptr;    //Pointer to a "memory BIO" structure holding our base64 data.
    b64_bio = BIO_new(BIO_f_base64());                      //Initialize our base64 filter BIO.
    mem_bio = BIO_new(BIO_s_mem());                           //Initialize our memory sink BIO.
    BIO_push(b64_bio, mem_bio);            //Link the BIOs by creating a filter-sink BIO chain.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);  //No newlines every 64 characters or less.
    BIO_write(b64_bio, b64_encode_this, encode_this_many_bytes); //Records base64 encoded data.
    BIO_flush(b64_bio);   //Flush data.  Necessary for b64 encoding, because of pad characters.
    BIO_get_mem_ptr(mem_bio, &mem_bio_mem_ptr);  //Store address of mem_bio's memory structure.
    BIO_set_close(mem_bio, BIO_NOCLOSE);   //Permit access to mem_ptr after BIOs are destroyed.
    BIO_free_all(b64_bio);  //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    BUF_MEM_grow(mem_bio_mem_ptr, (*mem_bio_mem_ptr).length + 1);   //Makes space for end null.
    (*mem_bio_mem_ptr).data[(*mem_bio_mem_ptr).length] = '\0';  //Adds null-terminator to tail.
    return (*mem_bio_mem_ptr).data; //Returns base-64 encoded data. (See: "buf_mem_st" struct).
}

char *base64decode (const void *b64_decode_this, int decode_this_many_bytes){
    BIO *b64_bio, *mem_bio;      //Declares two OpenSSL BIOs: a base64 filter and a memory BIO.
    char *base64_decoded = calloc( (decode_this_many_bytes*3)/4+1, sizeof(char) ); //+1 = null.
    b64_bio = BIO_new(BIO_f_base64());                      //Initialize our base64 filter BIO.
    mem_bio = BIO_new(BIO_s_mem());                         //Initialize our memory source BIO.
    BIO_write(mem_bio, b64_decode_this, decode_this_many_bytes); //Base64 data saved in source.
    BIO_push(b64_bio, mem_bio);          //Link the BIOs by creating a filter-source BIO chain.
    BIO_set_flags(b64_bio, BIO_FLAGS_BASE64_NO_NL);          //Don't require trailing newlines.
    int decoded_byte_index = 0;   //Index where the next base64_decoded byte should be written.
    while ( 0 < BIO_read(b64_bio, base64_decoded+decoded_byte_index, 1) ){ //Read byte-by-byte.
        decoded_byte_index++; //Increment the index until read of BIO decoded data is complete.
    } //Once we're done reading decoded data, BIO_read returns -1 even though there's no error.
    BIO_free_all(b64_bio);  //Destroys all BIOs in chain, starting with b64 (i.e. the 1st one).
    return base64_decoded;        //Returns base-64 decoded data with trailing null terminator.
}
*/