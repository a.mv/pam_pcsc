#ifndef _PCSC_H
#define _PCSC_H

#ifdef __APPLE__
#include <PCSC/winscard.h>
#include <PCSC/wintypes.h>
#else
#include <winscard.h>
#endif

#include "config.h"

#define CARD_MAX_ID_LEN 128
#define PCSC_BUFFER_LEN 258

#define METHOD_ID	0
#define METHOD_MFUL	1


#define LINE_BUFFER_LEN 16384
#define FIELD_SEPARATOR ":"

typedef struct pscs_context
{
	char Reader[128];
	SCARDHANDLE hCard;
	SCARD_IO_REQUEST pioSendPci;
	SCARDCONTEXT hContext;
} PCSC_CONTEXT;

typedef struct card_id
{
	BYTE *uid_bytes;
	unsigned int uid_len;
	char *UID;
	BYTE *atr_bytes;
	unsigned int atr_len;

	unsigned int block_size;
	unsigned int data_size;
	/*BYTE *Blocks;
	unsigned int nbBlocks;*/
	char *Blocks;
} CARD_ID;

int pcsc_init_ctx(int reader_n, PCSC_CONTEXT * pcsc);
int pcsc_free_ctx(PCSC_CONTEXT * pcsc);
size_t pcsc_read_card(PCSC_CONTEXT * pcsc,const char * apdu_s, BYTE ** read_data);
CARD_ID pcsc_read_card_id(PCSC_CONTEXT * pcsc);
int pcsc_read_data(PCSC_CONTEXT * pcsc,CARD_ID id, char ** data, size_t* size);
int pcsc_read_registry(CARD_ID * id);
void free_card_id(CARD_ID);


#endif
