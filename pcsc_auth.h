#ifndef _RFID_AUTH_H
#define _RFID_AUTH_H


typedef struct auth_struct
{
	char method[8];
	char* data;
} AUTH_STRUCT;

int pcsc_auth_user(const int reader_n, const char * user,const char * service,char ** password);
int rfid_auth_user (const int reader_n, const char * user,const char * service);

#endif
