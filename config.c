#include <string.h>
#include <libconfig.h>
#include "log.h"
#include "config.h"
#include "pcsc.h"

char * APDU_GET_ID;
char * APDU_READ_BLOCK;
char * APDU_WRITE_BLOCK;
char * REG_FILE=NULL;
char * IDP_FILE=NULL;
int READER_N=0;

int config_readfile(char* file){
  config_t cfg;
  config_setting_t *reader;
  config_setting_t *apdu;
  char str[256];
  const char *reader_name;
  const char *confstr;
  int ret;

  config_init(&cfg);
  ret=1;
  reader=NULL;
  /* Read the file. If there is an error, report it and exit. */
  if(! config_read_file(&cfg, file))
  {
    log_debug("%s:%d - %s\n", config_error_file(&cfg),
            config_error_line(&cfg), config_error_text(&cfg));
    ret=0;
  }

  if(ret && config_lookup_string(&cfg, "pam_reader", &reader_name)) {
    log_debug("Reader name: %s\n", reader_name);
  } else if (ret) {
    log_info("No pam_reader setting in configuration file %s.\n",file);
	ret=0;
  }

  if(ret && config_lookup_string(&cfg, "registry", &confstr)) {
		log_debug("Registry file: %s\n", confstr);
		REG_FILE=malloc((strlen(confstr)+1)*sizeof(char));
		strcpy(REG_FILE,confstr);
  } else {
	  log_info("Registry file not configured\n", NULL);
  }

  if(ret && config_lookup_string(&cfg, "idp", &confstr)) {
		log_debug("Registry file: %s\n", confstr);
		IDP_FILE=malloc((strlen(confstr)+1)*sizeof(char));
		strcpy(IDP_FILE,confstr);
  }

  if (ret) {
	strcpy(str,reader_name);
	reader = config_lookup(&cfg, str);
  }
  if(reader != NULL)
  {
	ret=ret && config_setting_lookup_int(reader, "pcsc_num", &READER_N);
	strcat(str,".apdu");
	apdu = config_lookup(&cfg, str);
	if(apdu != NULL && ret != 0 )
	{
		ret=ret & config_setting_lookup_string(apdu, "get_uid", &confstr);
		APDU_GET_ID=malloc((strlen(confstr)+1)*sizeof(char));
		strcpy(APDU_GET_ID,confstr);
		ret=ret & config_setting_lookup_string(apdu, "read_block", &confstr);
		APDU_READ_BLOCK=malloc((strlen(confstr)+1)*sizeof(char));
		strcpy(APDU_READ_BLOCK,confstr);
		ret=ret & config_setting_lookup_string(apdu, "write_block", &confstr);
		APDU_WRITE_BLOCK=malloc((strlen(confstr)+1)*sizeof(char));
		strcpy(APDU_WRITE_BLOCK,confstr);
	}

  } else if (ret) {
	  log_info("Reader %s is not found in configuration file %s.\n",reader_name,file);
	  ret=0;
  }
  if(ret == 1) log_debug("Config summary:\nreader_n=%d\nAPDU_GET_ID='%s'\nAPDU_READ_BLOCK='%s'\nAPDU_WRITE_BLOCK='%s'\n",READER_N,APDU_GET_ID,APDU_READ_BLOCK,APDU_WRITE_BLOCK);
  else {
	APDU_GET_ID="ffca000000";
	APDU_READ_BLOCK="ffb000%s10";
	APDU_WRITE_BLOCK="ffd600%s04";
	log_debug("Default config:\nreader_n=%d\nAPDU_GET_ID=%s\nAPDU_READ_BLOCK=%s\nAPDU_WRITE_BLOCK=%s\n",READER_N,APDU_GET_ID,APDU_READ_BLOCK,APDU_WRITE_BLOCK);
  }

  config_destroy(&cfg);
  return ret;
}