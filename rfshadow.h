#ifndef _RFSHADOW_H
#define _RFSHADOW_H

#include "pcsc.h"
#include "pcsc_auth.h"

#define LINE_BUFFER_LEN 16384
#define FIELD_SEPARATOR ":"
#define DATA_SEPARATOR "\t"


FILE* rfshadow_openfile(const char* user);
size_t rfshadow_getline(FILE*ptr_file, const char*user, const char*service, const CARD_ID card_id, AUTH_STRUCT* auth);
char* idp_getline (const CARD_ID ID);



#endif
